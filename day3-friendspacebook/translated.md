Current Status
==============

Letting it run for a pretty long time I have this output so far:

``` sh
(base) styty@nirn:~/git/google-beginners-2019/day3-friendspacebook$ python vm.py program
Running ....
http://emoji-t0anaxnr3nacpt4na.web.ctfco
```

I assume it will continue to run for a long time, so let's work on reversing it
like they actually want us to.

Ops Legend
==========

'🍡': add
'🤡': clone
'📐': divide
'😲': if_zero
'😄': if_not_zero
'🏀': jump_to
'🚛': load
'📬': modulo
'⭐': multiply
'🍿': pop
'📤': pop_out
'🎤': print_top
'📥': push
'🔪': sub
'🌓': xor
'⛰': jump_top
'⌛': exit


Details
-------

'🍡': add
Pop two items from stack, sum them, push result onto stack.

'🤡': clone
Pushes another copy of top of stack to stack.

'📐': divide
Pop to a, then pop to b, then push b // a.

'😲': if_zero
If the top value on stack is zero, execute until endif ('😐') or one of
['🏀', '⛰'].  If not zero, skip until endif ('😐')

'😄': if_not_zero
If the top value on stack is not zero, execute until endif ('😐') or one of
['🏀', '⛰'].  If zero, skip until endif ('😐')

'🏀': jump_to
Jump to label that follows.

'🚛': load
Loads data using recursive formula into specified accumulator.

'📬': modulo
Pop to a, then pop to b, then push b % a.

'⭐': multiply
Pops and multiplies top two items on stack, then pushes result to stack.

'🍿': pop
Pops from stack and stores in specified accumulator.

'📤': pop_out
Pops from stack and does nothing with item.

'🎤': print_top
Pops from stack and prints out chr(item).

'📥': push
Pushes specified accumulator onto stack.

'🔪': sub
Pop to a, then pop to b, then push b - a.

'🌓': xor
XORs the top two items on stack and pushes result onto stack.

'⛰': jump_top
Pops from stack and jumps to that location.

'⌛': exit
Prints "Done." and exits.


Patterns
========

'🥇': Accumulator 1
'🥈': Accumulator 2


Label Symbol Legend
===================

'🎌' = J
'🏁' = C
'💠' = B
'🔶' = O
'🚩' = F


Translation
===========

1 - 41 loading data onto stack using
🚛 🥇 0️⃣ ✋ 📥 🥇 pattern
Load blue numbers (according to formula) into accum1, push accum1
onto stack. Recursive formula: num = num * 10 + (ord(blue_num) - ord('0'))
This actually ends up being a direct translation of the blue number, it is what
it looks like.  1️⃣ 6️⃣ 2️⃣ 8️⃣ 5️⃣  represents 16,285 = 0x3f9d.

42 load '1' accum2

44 LABEL: BOJFC
44 pop accum1
44 push accum2
44 push accum1
44 load '389' accum1
45 push accum1
45 push accum2
46 jump to CFJBO
47 XOR
47 print
48 load '1' accum1
48 push accum1
48 add
48 pop accum2
49 if not zero
49   jump to BOJFC
49 endif

51 - 64 loading data onto stack using
🚛 🥇 0️⃣ ✋ 📥 🥇 pattern

65 load '99' accum2

67 LABEL: BCJOF
67 pop accum1
67 push accum2
67 push accum1
67 load '568' accum1
68 push accum1
68 push accum2
69 jump to CFJBO
70 XOR
70 print
71 load '1' accum1
71 push accum1
71 add
71 pop accum2
72 if not zero
72   jump to BCJOF
72 endif

74 - 105 loading data onto stack using
🚛 🥇 0️⃣ ✋ 📥 🥇 pattern

106 load '765' into accum2

108 LABEL: FBJOC
108 pop accum1
108 push accum2
108 push accum1
108 load '1023' accum1
109 push accum1
109 push accum2
110 jump to CFJBO
111 XOR
111 print
112 load '1' accum1
112 push accum1
112 add
112 pop accum2
113 if not zero
113   jump to FBJOC
113 endif
114 exit

#### Put 2 onto stack, accum1 = 2
116 LABEL: CFJBO
117 load '2' accum1
117 push accum1

#### Jump to FOCJB
117 LABEL: BJCFO
118 jump to FOCJB

119 LABEL: OJFBC
119 if zero
119   pop out
119   jump to OFBCJ
119   (halt load?)
119 endif
120 pop out
120 jump to JCBOF

121 LABEL JCFOB
121 if zero
121   pop out
121   jump to OFBCJ
121 endif
122 pop out
122 pop accum1
122 load '1' accum2
122 push accum2
122 sub
123 if zero
123   pop out
123   pop accum2
123   push accum1
123   push accum2
123   jump top
123 endif
123 push accum1

#### Top of stack += 1, accum2 = 1
124 LABEL OFBCJ
124 load '1' accum2
124 push accum2
124 add
124 jump to BJCFO

#### Stack top is cloned, 2 pushed on top
126 LABEL FOCJB
127 clone
127 load '2' accum1
127 push accum1

128 LABEL JFBOC
128 sub
128 if zero
128   pop out
128   load '1' accum1
128   push accum1
129   jump OJFBC
129 endif
130 pop out
130 clone
130 push accum1
131 modulo
131 if zero
131   jump OJFBC
131 endif
132 pop out
132 clone
132 push accum1
132 load '1' accum1
133 push accum1
133 add
133 clone
133 pop accum1
133 jump JFBOC

135 LABEL: JCBOF
136 clone
136 clone
136 load '0' accum2
136 push accum2

137 LABEL: CBOFJ
137 load '10' accum1
137 push accum1
138 multiply
138 pop accum2
138 push accum1
138 modulo
139 push accum2
139 add
139 pop accum2
139 pop accum1
139 clone
139 push accum2
139 sub
140 if zero
140   pop out
140   load '1' accum2
140   push accum2
140   jump JCFOB
140 endif
141 pop out
141 push accum1
141 load '10' accum1
141 push accum1
141 divide
142 if zero
142   jump JCFOB
142 endif
143 clone
143 push accum2
143 jump CBOFJ
