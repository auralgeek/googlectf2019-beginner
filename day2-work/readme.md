Hands a domain and port, let's connect.  Gives a shell sort of.  Pretty
gimped.  Uses busybox and alpine.  Cat not available cause 2easy.  `ls -la /bin`
gives:

```
> ls -la /bin
total 808
drwxr-xr-x    2 65534    65534         4096 Jun 13 14:28 .
drwxr-xr-x   20 0        0             4096 Jun 13 14:28 ..
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 arch -> /bin/busybox
-rwxr-xr-x    1 65534    65534       796240 Jan 24 07:45 busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 chgrp -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 chown -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 conspy -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 date -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 df -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 dmesg -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 dnsdomainname -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 dumpkmap -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 echo -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 false -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 fdflush -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 fsync -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 getopt -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 hostname -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 ionice -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 iostat -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 ipcalc -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 kill -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 login -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 ls -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 lzop -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 makemime -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 mkdir -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 mknod -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 mktemp -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 mount -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 mountpoint -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 mpstat -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 netstat -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 nice -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 pidof -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 ping -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 ping6 -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 pipe_progress -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 printenv -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 ps -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 pwd -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 reformime -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 rm -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 rmdir -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 run-parts -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 setpriv -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 setserial -> /bin/busybox
-r-sr-xr-x    1 1338     1338         19936 Jun 13 12:48 shell
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 sleep -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 stat -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 stty -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 sync -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 tar -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 true -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 umount -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 uname -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 usleep -> /bin/busybox
lrwxrwxrwx    1 65534    65534           12 May  9 20:49 watch -> /bin/busybox
```

Nothing I can see of value there... doing the same for /usr/bin:

```
```

Some of those are sized, probably the more interesting ones.  Poking through
them yields one with more potential `iconv` which converts text between
encodings.  Let's use it on the `README.flag` in the `/challenge` directory.

```
> iconv -f ASCII -t ASCII README.flag
CTF{4ll_D474_5h4ll_B3_Fr33}
```

Win.  What about `ORME.flag`?

```
> iconv -f ASCII -t ASCII ORME.flag
iconv: ORME.flag: Permission denied
```

Odd.  Should work, right?  What if we use `setpriv` from `/bin`?

```
> setpriv --nnp "/usr/bin/iconv -f ASCII -t ASCII ORME.flag"
setpriv: can't execute '"/usr/bin/iconv': No such file or directory
```

Can't seem to get it to work... ought to revisit this.
