Gives a pdf picture of satellite named "Osmium", and a binary "init_sat".  Run
the binary, get asked for a satellite name, hand it "Osmium", spits out
a weblink:

https://docs.google.com/document/d/14eYPluD_pi3824GAFanS29tWdTcKxP_XUxx7e303-3E

Weblink has a base64 string, decode that to get the following:

Username: wireshark-rocks
Password: start-sniffing!

Guess we should be sniffing for something.  No networks, so a pcap file?  The
binary?  The pdf?  Surprisingly wireshark ingests and parses ELF files.

`strings -t x | rg CTF` spits out:

```
 144b79 unixpacketunknown pc  of size   (targetpc= gcwaiting= gp.status= heap_live= idleprocs= in status  m->mcache= mallocing= ms clock,  nBSSRoots= p->mcache= p->status= s.nelems=  schedtick= span.list=/dev/stderr/dev/stdout30517578125: frame.sp=<invalid opCTF{\S{40}}GOTRACEBACKHOSTALIASESIdeographicInstCaptureInstRuneAnyLOCALDOMAINNew_Tai_LueOld_PersianPau_Cin_HauRES_OPTIONSSignWritingSoft_DottedWarang_CitiWhite_Space[:^xdigit:]additionalsauthoritiesbad addressbad messagebad timedivbroken pipecgocall nilclobberfreecreated by file existsfinal tokenfloat32nan2float64nan2float64nan3gccheckmarki/o timeoutlost mcachemSpanManualmethodargs(nil contextraw-controlruntime: P runtime: p scheddetailsetnonblockshort writetime: file tracealloc(unreachable [recovered] allocCount  found at *( gcscandone  m->gsignal= minTrigger= nDataRoots= nSpanRoots= pages/byte
 14c09c runtime.SetFinalizer: pointer not at beginning of allocated blockstrconv: internal error: extFloat.FixedDecimal called with n == 0Can't connect to the satellite. Please contact the Google CTF team.go package net: built with netgo build tag; using Go's DNS resolver
```

0x144b79 looks interesting.

Disconnected wifi and tried binary again, didn't work.  It opens a connection.
Stared at traffic over wireshark when opening binary and saw address
34.76.101.29 on port 1337.  Pretty obvious.  Scrub tier stuff loves to use
netcat so I try:

```
nc 34.76.101.29 1337
```

And it gives the same prompt the binary gives when you enter "Osmium".  I hit
"a" to display config data and it spits out same thing but with password
populated with flag:

CTF{4efcc72090af28fd33a2118985541f92e793477f}
